import { h, createApp } from 'vue';
import singleSpaVue from 'single-spa-vue';
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import 'vuetify/styles'
import App from './App.vue';
import { createVuetify } from 'vuetify/lib/framework.mjs';
import {router} from './router';

const vuetify = createVuetify({
  components:{
    ...components
  },
  directives,
});

const vueLifecycles = singleSpaVue({
  createApp: (...args) => {
    const vueInstance = createApp(...args);
    vueInstance.use(vuetify);
    vueInstance.use(router);
    return vueInstance;
  }
,
  appOptions: {
    render() {
      return h(App, {
        // single-spa props are available on the "this" object. Forward them to your component as needed.
        // https://single-spa.js.org/docs/building-applications#lifecycle-props
        // if you uncomment these, remember to add matching prop definitions for them in your App.vue file.
        /*
        name: this.name,
        mountParcel: this.mountParcel,
        singleSpa: this.singleSpa,
        */
      });
    },
  },
});

export const bootstrap = vueLifecycles.bootstrap;
export const mount = vueLifecycles.mount;
export const unmount = vueLifecycles.unmount;
