import { createRouter, createWebHistory } from 'vue-router';

export const router = createRouter({
    routes:[
      {
        path:"/",
        component: () => import('@/components/Spa-Home.vue')
      },
      {
        path:"/about",
        component:() => import('@/components/Spa-About.vue')
      },
      {
        path:"/contact",
        component:() => import('@/components/Spa-Contact.vue')
      }
    ],
    history: createWebHistory(),
});
